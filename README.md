# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://zalaeric@bitbucket.org/zalaeric/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/zalaeric/stroboskop/commits/c45c805b7951c98e4f2eb4c3b0d18af2f499142e

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/zalaeric/stroboskop/commits/a4c671bd3affa6a43148b18525f35d851ef69626

Naloga 6.3.2:
https://bitbucket.org/zalaeric/stroboskop/commits/ebda3471d7b2de3505908f5fd7761b0fefabba74

Naloga 6.3.3:
https://bitbucket.org/zalaeric/stroboskop/commits/420d516d6f217236a9314a6718850098918326cd

Naloga 6.3.4:
https://bitbucket.org/zalaeric/stroboskop/commits/73c50298b492fb53328d6d9adcff8f7bd6a656c5

Naloga 6.3.5:

```
git checkout master
git merge izgled
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/zalaeric/stroboskop/commits/72c00df62e792b50703c83c4bad946f045c94d8e

Naloga 6.4.2:
https://bitbucket.org/zalaeric/stroboskop/commits/b4c30348a712484f39fabd8ecb46809b9e96d0f3

Naloga 6.4.3:
https://bitbucket.org/zalaeric/stroboskop/commits/479e3e56577d05fc62fcdd77eb38a461e1aa4e1e

Naloga 6.4.4:
https://bitbucket.org/zalaeric/stroboskop/commits/82bc91b876e7b4c363150b36e4725c20cc8e273f